# VIDA Scope of Knowledge

This document is a documentation of the scope of the [Vaccine Information Distribution Assistant (VIDA)](https://gitlab.com/senti-techlabs-inc-public/cx-competition-vida/) built by [Senti AI](https://senti.ai) for the [Dialogflow CX Competition](https://events.withgoogle.com/dialogflow-cx-competition-global/).

## Table of Contents
1. [FAQs](#faqs)
    - [General Vaccine FAQs](#general-vaccine-faqs)
        - [Definitions](#definitions)
        - [General](#general)
        - [Mythbusters](#mythbusters)
    - [Specific Vaccine FAQs](#specific-vaccine-faqs)
2. [No-match and no-input handlers](#no-match-and-no-input-handlers)
3. [Placeholder Responses](#placeholder-responses)
4. [Vaccine Rollout Updates](#vaccine-rollout-updates)
5. [Vaccination Registration](#vaccination-registration)
6. [Check Vaccination Schedule](#check-vaccination-schedule)

## FAQs

With the aim of battling misinformation, the FAQs below were sourced from the website of the [World Health Organization](https://www.who.int).

### General Vaccine FAQs
These FAQs cater to COVID-19 vaccination in general. **Phrases are not limited to the examples listed below.**

#### Definitions
- **SARS-CoV-2 vs COVID-19**
- **SAGE**
- **EUL**
- **GACVS**

#### General

- **Vaccine existence** (e.g. Are there vaccines for COVID-19?)
- **Vaccine protection** (e.g. How long will vaccines protect me?)
- **Pandemic impact** (e.g. How quickly will vaccines stop the pandemic?)
- **Vaccine types** (e.g. What materials are used in vaccines?)
- **Vaccine mixing** (e.g. Is it okay to have a different brand on my second dose?)
- **Vaccine side effects** (e.g. Is it normal to feel pain after vaccination?)
- **Allergic reactions** (e.g. Can the vaccine trigger an allergic reaction?)
- **Adverse events** (e.g. What happens if injuries happen from the vaccine?)
- **Vaccine recall** (e.g. When do vaccine recalls happen?)
- **Safety monitoring** (e.g. Who is responsible for ensuring the vaccine's safety?)
- **Vaccination in children** (e.g. Is it safe for children to get vaccinated?)

#### Mythbusters
- **Non-COVID Vaccines and COVID-19** (e.g. Will a flu shot protect me against COVID-19?)
- **Vaccine benefits** (e.g. Does the vaccine help at all?)
- **Vaccination during menstruation** (e.g. Is it okay to get vaccinated on my period?)
- **Protocols after vaccination** (e.g. Is it fine to not wear a mask after I get the jab?)
- **Post-infection Vaccination** (e.g. Why do I need to get the jab if I already recovered from COVID-19?)
- **Infection after vaccination** (e.g. Is it possible to get infected even after getting the jab?)
- **Vaccination and positive tests** (e.g. Is it possible for the vaccine to cause a positive test result?)
- **Side effect factors** (e.g. Are vaccine side effects much worse for elderly people?)
- **Absence of side effects** (e.g. If I didn't get any side effects, is the vaccine not working?)
- **Dietary requirements** (e.g. Is fasting required before immunization?)
- **Vaccines and blood tinners** (e.g. Is the vaccine safe for those who take blood thinners?)
- **mRNA vaccine safety** (e.g. Do mRNA vaccines change a person's DNA?)
- **Vaccines and heart inflammation** (e.g. How likely will I develop myocarditis from the vaccine?)
- **Vaccines and blood clots** (e.g. Do vaccines cause TTS?)

### Specific Vaccine FAQs
These FAQs cater to specific brands of vaccines. The vaccines that are currently supported in this FAQ are Oxford-AstraZeneca, Janssen, Moderna, Pfizer-BioNTech, Sinopharm, and Sinovac. **Phrases are not limited to the examples listed below.**

- **Priority groups** (e.g. Who should be prioritized to receive `vaccine-brand`?)
- **Vaccination during pregnancy** (e.g. Is it safe to be vaccinated with `vaccine-brand` during pregnancy?)
- **Vaccination eligibility** (e.g. Am I qualified to get `vaccine-brand`?)
- **Recommended dosage and interval** (e.g. If I got my first dose of `vaccine-brand` today, when's the next one?)
- **Safety** (e.g. How safe is `vaccine-brand`?)
- **Efficacy** (e.g. What's the efficacy rate of `vaccine-brand`?)
- **Effectiveness against variants** (e.g. How does `vaccine-brand` hold up against the delta variant?)
- **Prevention against infection and transmission** (e.g. Will `vaccine-brand` prevent others from catching COVID-19 from me?)

## No-match and no-input handlers
When users type in unexpected responses, a No-match handler will be triggered. For blank responses, a No-input handler will be triggered. Such handlers may be found under Event Handlers and have names `sys.no-match 1`, `sys.no-match 2`, and `sys.no-match 3` respectively (or `sys.no-input`). These handlers keep the conversation on track and may be found on different parts of the conversation.

`sys.no-match 1` is when the first time the user makes an unexpected type of response. The bot will prompt the user to try again. The action is similar in `sys.no-match 2`. When the user does this for the third time, the bot will either route back to another part of the conversation (e.g. Main Menu) or end the conversation.

## Placeholder Responses
Some responses are designed to provide dynamic information to the user (i.e. using a webhook). To accommodate such responses, placeholder responses are provided for some parts. Such responses may contain words with square brackets around them, such as `[WEBSITE URL]`.

## Vaccine Rollout Updates
Vaccine Rollout Updates are also included and asks for the user's `city` first (not `country`, since this bot is designed to cater to a region or country only). No checking is done due to the absence of fulfillment, so only a placeholder response is provided:

> As of `[UPDATE DATE]`, `city` has fully vaccinated `[PERCENTAGE]` of its citizens and has given the first dose to `[PERCENTAGE]` of its citizens. 
> `[NUMBER]` of people are being vaccinated in a day, and `[PRIORITY GROUP]` are currently prioritized for vaccination.
> At this rate, we estimate that all citizens will be vaccinated by `[ESTIMATE DATE]`.

This feature must have webhook enabled when used in production to display the appropriate information.

## Vaccination Registration
The Vaccination Registration covers the data collection for the user's `full-name`, `date-of-birth`, `biological-sex`, `contact-number`, occupation (`health-care-worker-status`, meaning either a health care worker or not), `comorbidities`, and captured information confirmation prior to registration.

Webhooks are not enabled for this section but must be added when used in production.

## Check Vaccination Schedule
The Check Vaccination Schedule includes checking whether a user has been registered for vaccination already or not. Similar to the Vaccination Registration process above, webhooks are not enabled but must be added when used in production.