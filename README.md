# Vaccine Information Distribution Assistant (VIDA)
[![LICENSE](https://img.shields.io/badge/license-Apache%202-blue)](LICENSE.md)


## Table of Contents
1. [Overview](#overview)
    - [Conversation Design](#conversation-design)
2. [Getting Started](#getting-started)
    - [Prereqeuisites](#prerequisites)
    - [Installation](#installation)
3. [Usage](#usage)
    - [Collaboration](#collaboration)
    - [Fulfillment and Integration](#fulfillment-and-integration)
4. [Contributing](#contributing)
5. [License](#license)
6. [Contact](#contact)
7. [Acknowledgements](#acknowledgements)

## Overview
This repository serves as an entry of [Senti AI](https://senti.ai) to the Track 1 (UX Designers) of the [Dialogflow CX Competition](https://events.withgoogle.com/dialogflow-cx-competition-global/). The scope can be found in [this document](SCOPE.md).

**VIDA leverages Dialogflow Messenger on Dialogflow CX.** For more information on how to use UI components of Dialogflow Messenger, please refer to the [documentation](https://cloud.google.com/dialogflow/cx/docs/concept/integration/dialogflow-messenger).

VIDA can:
- Answer general questions about vaccines.
- Answer questions specific to certain vaccine types and brands (i.e. AstraZeneca, Janssen, Moderna, Pfizer-BioNTech, Sinopharm, and Sinovac).
- Give updates on your location's vaccine rollout (no webhook).
- Register you to a vaccination slot (no webhook).
- Check your vaccination appointment schedule (no webhook).

*As the aim of this project is to combat misinformation, all of VIDA's responses on FAQs uses the [World Health Organization](https://www.who.int) FAQs as a source. For more details on VIDA's scope of knowledge, please read our [Scope](SCOPE.md).*

As a developer, you can:
- Add more flows, widen VIDA's scope of knowledge, or update FAQs as new information becomes available.
- Use webhooks to add fulfillment to some of VIDA's features. Some responses are given placeholder responses such as `[WEBSITE URL]` to accommodate dynamic responses in production. *Please see [Fulfillment and Integration](#fulfillment-and-integration).*
- Capture more important information from the user by adding additional questions or using other technologies such as ID scanning for verification purposes.
- Add additional verification layers, such as an OTP.
- Deploy this project to production. Apart from Dialogflow Messenger, other messaging platforms such as Facebook Messenger and Viber may be used.

### Assumptions
- The entry focuses only on the conversation design aspect of the bot.
- Fulfillment an deployment is not covered in the scope.
- This bot does not cater to a specific region or country but is designed to be used that way. For instance, some parts only ask about the user's `city`.

### Conversation Design
The conversation design utilizes a main menu that projects a clear picture of what the current capabilities of VIDA are for the user to immediately see. This setup can be easily customized by developers.

For other parts such as FAQs, users are given the freedom to type their own input with minimal guidance. No match input and No input handlers were added to the conversation to help the user stay on track during the conversation.

## Getting Started

### Prerequisites
To be able to use this project, you will need to:
- Create a Google Cloud project
- Enable billing
- Enable Dialogflow API
- Create a Dialogflow CX agent

For more information on setting up your agent, please refer to the [Dialogflow CX Quickstarts](https://cloud.google.com/dialogflow/cx/docs/quick).

### Installation
1. **Clone or download this repository.** If you downloaded a `.zip` file, unzip or extract it first.
2. Go to the [Dialogflow CX Console homepage](https://dialogflow.cloud.google.com/cx/projects) and **select your GCP project**.

![Dialogflow CX Console homepage](resources/01.png)

3. In the list of agents within the project, **look for the CX agent that you have created and click the 3 dots on the right** (next to the Region column).

![Restore CX agent](resources/02.png)

4. Click **Restore**.
5. Click the **Upload** radio button and select the blob file (`exported_agent_senti-dialogflow-cx-competition-entry-2.blob`) from your computer. (You may also upload from Google Cloud Storage.)

![Upload local file](resources/03.png)

6. Click **Restore**.

## Usage

### Collaboration
This project is designed to be collaborated on by a team of up to 3 members using separate flows.

There are three flows with distinct purposes in this project:
- **Main Flow.** This contains the greetings spiel and the main menu of possible tasks that the chatbot can provide. The main features can be extended/added via this flow.
- **FAQs Flow.** This contains FAQs about general information on COVID-19 vaccines as well as those specifically per brand.
- **Vaccination Registration Flow.** This contains a sequence of questions to the user to register for vaccination.
- **Collect Information Flow.** This contains the actual sequence of questions for data collection needed in order to be successfully registered for vaccination.
- **Check Schedule Flow.** This serves as the user's reference on when and where their appointment is if the user has already finished their vaccination registration.

### Fulfillment and Integration
This project does not come with fulfillment apart from text-based responses and custom payloads (JSON) for Dialogflow Messenger. We recommend that this bot be connected other components such as databases and other APIs before using it in production.

The following parts of the conversation should be connected to a webhook when used in production:
- Vaccine Rollout Status `(Check City - Vaccine Rollout)`
- Vaccination Registration / Collect Information Flow
- Check Schedule Flow

### Testing
Testing may be done through **Test Agent** or **Dialogflow Messenger** (Integrations). Testing on **Test Agent**, however, does not show UI components and shows them as a custom payload (JSON) instead. **To start with the bot on Test Agent, say "Hello" or a similar greeting.**

![Test Agent on Dialogflow CX](resources/04.png)

To see the Dialogflow Mesesenger UI components in action, we recommend that you use Dialogflow Messenger to test VIDA out. To access this, go to the **Manage** tab (next to **Build**), go to **Integrations**, and under **Dialogflow Messenger**, click **Connect**.

![Integrations](resources/05.png)

Inside the Dialogflow Messenger window, click **Try it out**.

![Dialogflow Messenger](resources/06.png)

**Click on the button that appears on the right.** Once the chat window appears, **trigger the bot by saying "Hello"** or a similar greeting.

![Chatbot trigger](resources/07.png)

## Contributing
Contributions are welcome. For more information, please refer to the [Contribution Guidelines](CONTRIBUTING.md).

## License
This project is licensed under the terms of the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0).

## Acknowledgements
We would like to thank Google and the Dialogflow CX team for this awesome opportunity!

## Contact
For any concerns about the project, you may email us at:

cx-competition-support@senti.com.ph